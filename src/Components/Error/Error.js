import React from 'react'
import { useNavigate } from 'react-router-dom'

export default function Error() {

  const navigate = useNavigate()
  
  return (
    <div>
      <h1>404 Error la route demandée n'exsite pas !!</h1>
      <button onClick={() => navigate('/')}>Retour à l'accueil</button>
    </div>
  )
}
