import React from 'react'
import { Link, Outlet } from 'react-router-dom'

export default function Services() {
  return (
    <div>
        <nav>
            <Link to="/services/developpement">Developpement</Link>
            <Link to="/services/cybersecurite">cyber sécurité</Link>
        </nav>
        <Outlet /> //ne pas oublier d'ajouter cette ligne por charger les sous-routes
        <h1>Page des services</h1>
    </div>
  )
}
